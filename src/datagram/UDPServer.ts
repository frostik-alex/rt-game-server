import {RemoteInfo} from "dgram";
import {PORT} from "../config";
import ClientSlot from "./client/ClientSlot";
import {ClientStatus} from "./client/ClientStatus";

import UDPHostConnection from "./UDPHostConnection";

const N_MAX_CLIENTS = 64;

const PACKET_SIZE_BYTES = 1024;

export default class UDPServer{

    private connection: UDPHostConnection;


    private n_connectedClients: number = 0;
    private slots: Array<ClientSlot>;


    constructor() {
        this.connection = new UDPHostConnection(PORT, PACKET_SIZE_BYTES);

        this.connection.onMessageFunc = this.onMessage.bind(this);
        this.connection.onListeningFunc = this.onListening.bind(this);

        this.initClientSlots();
    }


    private findFreeSlot(): ClientSlot{
        let slot: ClientSlot;

        for(let id=0; id<N_MAX_CLIENTS; id++){
            slot = this.slots[id];

            if(slot.status === ClientStatus.DISCONNECTED){
                return slot;
            }
        }

        return null;
    }


    private initClientSlots():void{
        this.slots = new Array<ClientSlot>(N_MAX_CLIENTS);

        for(let i=0; i<N_MAX_CLIENTS; i++){
            this.slots[i] = new ClientSlot(i);
        }
    }


    private onMessage(message: Buffer, remote: RemoteInfo){
        console.log(`Message from ${remote.address}:${remote.port} - ${message.toString()}`);


        this.connection.sendPacketLoad(Buffer.from("SERVER GOT YOUR MESSAGE"), remote.address, remote.port);
    }


    private onListening(){
        let addressInfo:any = this.connection.socket.address();
        console.log(`Listening on ${addressInfo.address}:${addressInfo.port}`);
    }


    public rise(){
        this.connection.open();
    }
}