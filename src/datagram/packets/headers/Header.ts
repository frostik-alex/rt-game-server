import Packet from "../Packet";


export default abstract class Header{

    readonly offset: number;
    readonly size: number;


    protected constructor(offset: number, size: number) {
        this.offset = offset;
        this.size = size;
    }


    public abstract read(rcvBuffer: Buffer): number;
    public abstract write(sndBuffer: Buffer): number;
}
