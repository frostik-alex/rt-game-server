

//TODO: Think on Packet system instead of headers system



export default abstract class Packet {


    public abstract read(rcvBuf: Buffer): void;
    public abstract write(sndBuf: Buffer): void;


    public abstract getTypeCode(): number;
};



