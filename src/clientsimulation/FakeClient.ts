import {RemoteInfo, Socket} from "dgram";
import HeadersDescriptor from "../datagram/packets/headers/HeadersDescriptor";
import Header from "../datagram/packets/headers/Header";
import * as dgram from "dgram";
import CRC32Header from "../datagram/packets/headers/CRC32Header";


const PACKET_SIZE_BYTES = 1024;


export default class FakeClient {


    private socket: Socket;

    private hostAddr: string;
    private hostPort: number;

    public rcvBuffer: Buffer;
    public sndBuffer: Buffer;


    private headersDescriptor: HeadersDescriptor;


    constructor(hostAddr: string, port: number){

        this.rcvBuffer = Buffer.alloc(PACKET_SIZE_BYTES);
        this.sndBuffer = Buffer.alloc(PACKET_SIZE_BYTES);

        this.socket = dgram.createSocket("udp4");

        this.headersDescriptor = new HeadersDescriptor( this.rcvBuffer, this.sndBuffer );

        this.headersDescriptor.addHeader(
            new CRC32Header()
        );


        this.socket.on("message", this.innerMessageHandler.bind(this));

        this.hostAddr = hostAddr;
        this.hostPort = port;
    }


    private innerMessageHandler(plainPacketBuffer: Buffer, rinfo: RemoteInfo){
        // decline packets unable to read
        let loadDataBuffer: Buffer = this.headersDescriptor.validateReceivedPacket(plainPacketBuffer);

        // headers failed to validate.
        if( loadDataBuffer === null ){
            return;
        }


        console.log("Reply from server: ", loadDataBuffer.toString());
    }


    public sendMessage(loadData: Buffer){
        let sndPacketBuffer: Buffer = this.headersDescriptor.packDataWithHeaders(loadData);

        // failed to put packet load - too large load
        if( sndPacketBuffer === null ){
            return;
        }

        console.log(`Delivering to ${this.hostAddr}:${this.hostPort}. MSG: ${sndPacketBuffer.toString()}`);

        this.socket.send(sndPacketBuffer, this.hostPort, this.hostAddr);
    }
}
