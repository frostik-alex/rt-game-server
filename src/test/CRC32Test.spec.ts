import {crc32 as libCRC32} from "crc";
import {crc32} from "../crypto/CRC32"

import {expect} from 'chai';


describe("Should calculate CRC32 correct", () => {
    it("Should CRC32 full buffer", () => {

        let testData: string = "test data";

        let buf: Buffer = Buffer.from(testData);

        let libValue = libCRC32(buf);
        let myValue = crc32(buf, 0);

        expect(myValue).eqls(libValue);
    });

    it("Should CRC32 of split buffer", () => {

        let testData: string = "another test data";

        let buf: Buffer = Buffer.from(testData);

        let libValue = libCRC32(buf.slice(5));
        let myValue = crc32(buf, 5);

        expect(myValue).eqls(libValue);
    });

    it("Should CRC32 of same data buffer", () => {

        let testData: string = "another test data";

        let buf: Buffer = Buffer.from(testData);

        let libValue = libCRC32(Buffer.from(testData.slice(8)));
        let myValue = crc32(buf, 8);

        expect(myValue).eqls(libValue);
    });

});
